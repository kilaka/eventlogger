# eventlogger.client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>alik</groupId>
    <artifactId>eventlogger.client</artifactId>
    <version>0.1.0-SNAPSHOT</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "alik:eventlogger.client:0.1.0-SNAPSHOT"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/eventlogger.client-0.1.0-SNAPSHOT.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.*;
import io.swagger.client.auth.*;
import io.swagger.client.model.*;
import io.swagger.client.api.BasicErrorControllerApi;

import java.io.File;
import java.util.*;

public class BasicErrorControllerApiExample {

    public static void main(String[] args) {
        
        BasicErrorControllerApi apiInstance = new BasicErrorControllerApi();
        try {
            ModelAndView result = apiInstance.errorHtmlUsingDELETE();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling BasicErrorControllerApi#errorHtmlUsingDELETE");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://localhost:8080*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*BasicErrorControllerApi* | [**errorHtmlUsingDELETE**](docs/BasicErrorControllerApi.md#errorHtmlUsingDELETE) | **DELETE** /error | errorHtml
*BasicErrorControllerApi* | [**errorHtmlUsingGET**](docs/BasicErrorControllerApi.md#errorHtmlUsingGET) | **GET** /error | errorHtml
*BasicErrorControllerApi* | [**errorHtmlUsingHEAD**](docs/BasicErrorControllerApi.md#errorHtmlUsingHEAD) | **HEAD** /error | errorHtml
*BasicErrorControllerApi* | [**errorHtmlUsingOPTIONS**](docs/BasicErrorControllerApi.md#errorHtmlUsingOPTIONS) | **OPTIONS** /error | errorHtml
*BasicErrorControllerApi* | [**errorHtmlUsingPATCH**](docs/BasicErrorControllerApi.md#errorHtmlUsingPATCH) | **PATCH** /error | errorHtml
*BasicErrorControllerApi* | [**errorHtmlUsingPOST**](docs/BasicErrorControllerApi.md#errorHtmlUsingPOST) | **POST** /error | errorHtml
*BasicErrorControllerApi* | [**errorHtmlUsingPUT**](docs/BasicErrorControllerApi.md#errorHtmlUsingPUT) | **PUT** /error | errorHtml
*EventDefEntityApi* | [**deleteEventDefUsingDELETE**](docs/EventDefEntityApi.md#deleteEventDefUsingDELETE) | **DELETE** /eventDefs/{id} | deleteEventDef
*EventDefEntityApi* | [**eventDefuserUsingDELETE**](docs/EventDefEntityApi.md#eventDefuserUsingDELETE) | **DELETE** /eventDefs/{id}/user | eventDefuser
*EventDefEntityApi* | [**eventDefuserUsingGET**](docs/EventDefEntityApi.md#eventDefuserUsingGET) | **GET** /eventDefs/{id}/user | eventDefuser
*EventDefEntityApi* | [**eventDefuserUsingPATCH**](docs/EventDefEntityApi.md#eventDefuserUsingPATCH) | **PATCH** /eventDefs/{id}/user | eventDefuser
*EventDefEntityApi* | [**eventDefuserUsingPOST**](docs/EventDefEntityApi.md#eventDefuserUsingPOST) | **POST** /eventDefs/{id}/user | eventDefuser
*EventDefEntityApi* | [**eventDefuserUsingPUT**](docs/EventDefEntityApi.md#eventDefuserUsingPUT) | **PUT** /eventDefs/{id}/user | eventDefuser
*EventDefEntityApi* | [**findAllEventDefUsingGET**](docs/EventDefEntityApi.md#findAllEventDefUsingGET) | **GET** /eventDefs | findAllEventDef
*EventDefEntityApi* | [**findByUserIdEventDefUsingGET**](docs/EventDefEntityApi.md#findByUserIdEventDefUsingGET) | **GET** /eventDefs/search/findByUserId | findByUserIdEventDef
*EventDefEntityApi* | [**findOneEventDefUsingGET**](docs/EventDefEntityApi.md#findOneEventDefUsingGET) | **GET** /eventDefs/{id} | findOneEventDef
*EventDefEntityApi* | [**saveEventDefUsingPOST**](docs/EventDefEntityApi.md#saveEventDefUsingPOST) | **POST** /eventDefs | saveEventDef
*EventDefEntityApi* | [**saveEventDefUsingPUT**](docs/EventDefEntityApi.md#saveEventDefUsingPUT) | **PUT** /eventDefs/{id} | saveEventDef
*EventEntityApi* | [**deleteEventUsingDELETE**](docs/EventEntityApi.md#deleteEventUsingDELETE) | **DELETE** /events/{id} | deleteEvent
*EventEntityApi* | [**eventeventDefUsingDELETE**](docs/EventEntityApi.md#eventeventDefUsingDELETE) | **DELETE** /events/{id}/eventDef | eventeventDef
*EventEntityApi* | [**eventeventDefUsingGET**](docs/EventEntityApi.md#eventeventDefUsingGET) | **GET** /events/{id}/eventDef | eventeventDef
*EventEntityApi* | [**eventeventDefUsingPATCH**](docs/EventEntityApi.md#eventeventDefUsingPATCH) | **PATCH** /events/{id}/eventDef | eventeventDef
*EventEntityApi* | [**eventeventDefUsingPOST**](docs/EventEntityApi.md#eventeventDefUsingPOST) | **POST** /events/{id}/eventDef | eventeventDef
*EventEntityApi* | [**eventeventDefUsingPUT**](docs/EventEntityApi.md#eventeventDefUsingPUT) | **PUT** /events/{id}/eventDef | eventeventDef
*EventEntityApi* | [**findAllEventUsingGET**](docs/EventEntityApi.md#findAllEventUsingGET) | **GET** /events | findAllEvent
*EventEntityApi* | [**findOneEventUsingGET**](docs/EventEntityApi.md#findOneEventUsingGET) | **GET** /events/{id} | findOneEvent
*EventEntityApi* | [**saveEventUsingPOST**](docs/EventEntityApi.md#saveEventUsingPOST) | **POST** /events | saveEvent
*EventEntityApi* | [**saveEventUsingPUT**](docs/EventEntityApi.md#saveEventUsingPUT) | **PUT** /events/{id} | saveEvent
*InstantEventDefEntityApi* | [**deleteInstantEventDefUsingDELETE**](docs/InstantEventDefEntityApi.md#deleteInstantEventDefUsingDELETE) | **DELETE** /instantEventDefs/{id} | deleteInstantEventDef
*InstantEventDefEntityApi* | [**findAllInstantEventDefUsingGET**](docs/InstantEventDefEntityApi.md#findAllInstantEventDefUsingGET) | **GET** /instantEventDefs | findAllInstantEventDef
*InstantEventDefEntityApi* | [**findByUserIdInstantEventDefUsingGET**](docs/InstantEventDefEntityApi.md#findByUserIdInstantEventDefUsingGET) | **GET** /instantEventDefs/search/findByUserId | findByUserIdInstantEventDef
*InstantEventDefEntityApi* | [**findOneInstantEventDefUsingGET**](docs/InstantEventDefEntityApi.md#findOneInstantEventDefUsingGET) | **GET** /instantEventDefs/{id} | findOneInstantEventDef
*InstantEventDefEntityApi* | [**instantEventDefuserUsingDELETE**](docs/InstantEventDefEntityApi.md#instantEventDefuserUsingDELETE) | **DELETE** /instantEventDefs/{id}/user | instantEventDefuser
*InstantEventDefEntityApi* | [**instantEventDefuserUsingGET**](docs/InstantEventDefEntityApi.md#instantEventDefuserUsingGET) | **GET** /instantEventDefs/{id}/user | instantEventDefuser
*InstantEventDefEntityApi* | [**instantEventDefuserUsingPATCH**](docs/InstantEventDefEntityApi.md#instantEventDefuserUsingPATCH) | **PATCH** /instantEventDefs/{id}/user | instantEventDefuser
*InstantEventDefEntityApi* | [**instantEventDefuserUsingPOST**](docs/InstantEventDefEntityApi.md#instantEventDefuserUsingPOST) | **POST** /instantEventDefs/{id}/user | instantEventDefuser
*InstantEventDefEntityApi* | [**instantEventDefuserUsingPUT**](docs/InstantEventDefEntityApi.md#instantEventDefuserUsingPUT) | **PUT** /instantEventDefs/{id}/user | instantEventDefuser
*InstantEventDefEntityApi* | [**saveInstantEventDefUsingPOST**](docs/InstantEventDefEntityApi.md#saveInstantEventDefUsingPOST) | **POST** /instantEventDefs | saveInstantEventDef
*InstantEventDefEntityApi* | [**saveInstantEventDefUsingPUT**](docs/InstantEventDefEntityApi.md#saveInstantEventDefUsingPUT) | **PUT** /instantEventDefs/{id} | saveInstantEventDef
*PolarEventDefEntityApi* | [**deletePolarEventDefUsingDELETE**](docs/PolarEventDefEntityApi.md#deletePolarEventDefUsingDELETE) | **DELETE** /polarEventDefs/{id} | deletePolarEventDef
*PolarEventDefEntityApi* | [**findAllPolarEventDefUsingGET**](docs/PolarEventDefEntityApi.md#findAllPolarEventDefUsingGET) | **GET** /polarEventDefs | findAllPolarEventDef
*PolarEventDefEntityApi* | [**findByUserIdPolarEventDefUsingGET**](docs/PolarEventDefEntityApi.md#findByUserIdPolarEventDefUsingGET) | **GET** /polarEventDefs/search/findByUserId | findByUserIdPolarEventDef
*PolarEventDefEntityApi* | [**findOnePolarEventDefUsingGET**](docs/PolarEventDefEntityApi.md#findOnePolarEventDefUsingGET) | **GET** /polarEventDefs/{id} | findOnePolarEventDef
*PolarEventDefEntityApi* | [**polarEventDefuserUsingDELETE**](docs/PolarEventDefEntityApi.md#polarEventDefuserUsingDELETE) | **DELETE** /polarEventDefs/{id}/user | polarEventDefuser
*PolarEventDefEntityApi* | [**polarEventDefuserUsingGET**](docs/PolarEventDefEntityApi.md#polarEventDefuserUsingGET) | **GET** /polarEventDefs/{id}/user | polarEventDefuser
*PolarEventDefEntityApi* | [**polarEventDefuserUsingPATCH**](docs/PolarEventDefEntityApi.md#polarEventDefuserUsingPATCH) | **PATCH** /polarEventDefs/{id}/user | polarEventDefuser
*PolarEventDefEntityApi* | [**polarEventDefuserUsingPOST**](docs/PolarEventDefEntityApi.md#polarEventDefuserUsingPOST) | **POST** /polarEventDefs/{id}/user | polarEventDefuser
*PolarEventDefEntityApi* | [**polarEventDefuserUsingPUT**](docs/PolarEventDefEntityApi.md#polarEventDefuserUsingPUT) | **PUT** /polarEventDefs/{id}/user | polarEventDefuser
*PolarEventDefEntityApi* | [**savePolarEventDefUsingPOST**](docs/PolarEventDefEntityApi.md#savePolarEventDefUsingPOST) | **POST** /polarEventDefs | savePolarEventDef
*PolarEventDefEntityApi* | [**savePolarEventDefUsingPUT**](docs/PolarEventDefEntityApi.md#savePolarEventDefUsingPUT) | **PUT** /polarEventDefs/{id} | savePolarEventDef
*ProfileControllerApi* | [**listAllFormsOfMetadataUsingGET**](docs/ProfileControllerApi.md#listAllFormsOfMetadataUsingGET) | **GET** /profile | listAllFormsOfMetadata
*ProfileControllerApi* | [**profileOptionsUsingOPTIONS**](docs/ProfileControllerApi.md#profileOptionsUsingOPTIONS) | **OPTIONS** /profile | profileOptions
*RestServiceApi* | [**findAllUsingGET**](docs/RestServiceApi.md#findAllUsingGET) | **GET** /api | findAll
*RestServiceApi* | [**patchAllUsingPATCH**](docs/RestServiceApi.md#patchAllUsingPATCH) | **PATCH** /api | patchAll
*UserEntityApi* | [**deleteUserUsingDELETE**](docs/UserEntityApi.md#deleteUserUsingDELETE) | **DELETE** /users/{id} | deleteUser
*UserEntityApi* | [**findAllUserUsingGET**](docs/UserEntityApi.md#findAllUserUsingGET) | **GET** /users | findAllUser
*UserEntityApi* | [**findByFirstNameLikeUserUsingGET**](docs/UserEntityApi.md#findByFirstNameLikeUserUsingGET) | **GET** /users/search/findByFirstNameLike | findByFirstNameLikeUser
*UserEntityApi* | [**findByFirstNameUserUsingGET**](docs/UserEntityApi.md#findByFirstNameUserUsingGET) | **GET** /users/search/findByFirstName | findByFirstNameUser
*UserEntityApi* | [**findByUsernameUserUsingGET**](docs/UserEntityApi.md#findByUsernameUserUsingGET) | **GET** /users/search/findByUsername | findByUsernameUser
*UserEntityApi* | [**findOneUserUsingGET**](docs/UserEntityApi.md#findOneUserUsingGET) | **GET** /users/{id} | findOneUser
*UserEntityApi* | [**saveUserUsingPOST**](docs/UserEntityApi.md#saveUserUsingPOST) | **POST** /users | saveUser
*UserEntityApi* | [**saveUserUsingPUT**](docs/UserEntityApi.md#saveUserUsingPUT) | **PUT** /users/{id} | saveUser


## Documentation for Models

 - [Company](docs/Company.md)
 - [Event](docs/Event.md)
 - [EventDef](docs/EventDef.md)
 - [InstantEventDef](docs/InstantEventDef.md)
 - [Link](docs/Link.md)
 - [ModelAndView](docs/ModelAndView.md)
 - [PolarEventDef](docs/PolarEventDef.md)
 - [ResourceEvent](docs/ResourceEvent.md)
 - [ResourceEventDef](docs/ResourceEventDef.md)
 - [ResourceInstantEventDef](docs/ResourceInstantEventDef.md)
 - [ResourcePolarEventDef](docs/ResourcePolarEventDef.md)
 - [ResourceSupport](docs/ResourceSupport.md)
 - [ResourceUser](docs/ResourceUser.md)
 - [ResourcesEvent](docs/ResourcesEvent.md)
 - [ResourcesEventDef](docs/ResourcesEventDef.md)
 - [ResourcesInstantEventDef](docs/ResourcesInstantEventDef.md)
 - [ResourcesListEventDef](docs/ResourcesListEventDef.md)
 - [ResourcesListUser](docs/ResourcesListUser.md)
 - [ResourcesPolarEventDef](docs/ResourcesPolarEventDef.md)
 - [ResourcesUser](docs/ResourcesUser.md)
 - [User](docs/User.md)
 - [View](docs/View.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



