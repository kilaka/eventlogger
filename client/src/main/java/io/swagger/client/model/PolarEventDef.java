/*
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.User;
import java.io.IOException;

/**
 * PolarEventDef
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-10T12:59:05.805+02:00")
public class PolarEventDef {
  @SerializedName("id")
  private Long id = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("northName")
  private String northName = null;

  @SerializedName("southName")
  private String southName = null;

  @SerializedName("updateTime")
  private Long updateTime = null;

  @SerializedName("user")
  private User user = null;

  public PolarEventDef id(Long id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public PolarEventDef name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PolarEventDef northName(String northName) {
    this.northName = northName;
    return this;
  }

   /**
   * Get northName
   * @return northName
  **/
  @ApiModelProperty(value = "")
  public String getNorthName() {
    return northName;
  }

  public void setNorthName(String northName) {
    this.northName = northName;
  }

  public PolarEventDef southName(String southName) {
    this.southName = southName;
    return this;
  }

   /**
   * Get southName
   * @return southName
  **/
  @ApiModelProperty(value = "")
  public String getSouthName() {
    return southName;
  }

  public void setSouthName(String southName) {
    this.southName = southName;
  }

  public PolarEventDef updateTime(Long updateTime) {
    this.updateTime = updateTime;
    return this;
  }

   /**
   * Get updateTime
   * @return updateTime
  **/
  @ApiModelProperty(value = "")
  public Long getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Long updateTime) {
    this.updateTime = updateTime;
  }

  public PolarEventDef user(User user) {
    this.user = user;
    return this;
  }

   /**
   * Get user
   * @return user
  **/
  @ApiModelProperty(value = "")
  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PolarEventDef polarEventDef = (PolarEventDef) o;
    return Objects.equals(this.id, polarEventDef.id) &&
        Objects.equals(this.name, polarEventDef.name) &&
        Objects.equals(this.northName, polarEventDef.northName) &&
        Objects.equals(this.southName, polarEventDef.southName) &&
        Objects.equals(this.updateTime, polarEventDef.updateTime) &&
        Objects.equals(this.user, polarEventDef.user);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, northName, southName, updateTime, user);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PolarEventDef {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    northName: ").append(toIndentedString(northName)).append("\n");
    sb.append("    southName: ").append(toIndentedString(southName)).append("\n");
    sb.append("    updateTime: ").append(toIndentedString(updateTime)).append("\n");
    sb.append("    user: ").append(toIndentedString(user)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

