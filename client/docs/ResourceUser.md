
# ResourceUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | [**Company**](Company.md) |  |  [optional]
**firstName** | **String** |  |  [optional]
**id** | **Long** |  |  [optional]
**lastName** | **String** |  |  [optional]
**links** | [**List&lt;Link&gt;**](Link.md) |  |  [optional]
**username** | **String** |  |  [optional]



