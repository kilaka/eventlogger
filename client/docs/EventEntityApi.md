# EventEntityApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteEventUsingDELETE**](EventEntityApi.md#deleteEventUsingDELETE) | **DELETE** /events/{id} | deleteEvent
[**eventeventDefUsingDELETE**](EventEntityApi.md#eventeventDefUsingDELETE) | **DELETE** /events/{id}/eventDef | eventeventDef
[**eventeventDefUsingGET**](EventEntityApi.md#eventeventDefUsingGET) | **GET** /events/{id}/eventDef | eventeventDef
[**eventeventDefUsingPATCH**](EventEntityApi.md#eventeventDefUsingPATCH) | **PATCH** /events/{id}/eventDef | eventeventDef
[**eventeventDefUsingPOST**](EventEntityApi.md#eventeventDefUsingPOST) | **POST** /events/{id}/eventDef | eventeventDef
[**eventeventDefUsingPUT**](EventEntityApi.md#eventeventDefUsingPUT) | **PUT** /events/{id}/eventDef | eventeventDef
[**findAllEventUsingGET**](EventEntityApi.md#findAllEventUsingGET) | **GET** /events | findAllEvent
[**findOneEventUsingGET**](EventEntityApi.md#findOneEventUsingGET) | **GET** /events/{id} | findOneEvent
[**saveEventUsingPOST**](EventEntityApi.md#saveEventUsingPOST) | **POST** /events | saveEvent
[**saveEventUsingPUT**](EventEntityApi.md#saveEventUsingPUT) | **PUT** /events/{id} | saveEvent


<a name="deleteEventUsingDELETE"></a>
# **deleteEventUsingDELETE**
> deleteEventUsingDELETE(id)

deleteEvent

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventEntityApi;


EventEntityApi apiInstance = new EventEntityApi();
Long id = 789L; // Long | id
try {
    apiInstance.deleteEventUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling EventEntityApi#deleteEventUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="eventeventDefUsingDELETE"></a>
# **eventeventDefUsingDELETE**
> eventeventDefUsingDELETE(id)

eventeventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventEntityApi;


EventEntityApi apiInstance = new EventEntityApi();
Long id = 789L; // Long | id
try {
    apiInstance.eventeventDefUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling EventEntityApi#eventeventDefUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="eventeventDefUsingGET"></a>
# **eventeventDefUsingGET**
> ResourceEventDef eventeventDefUsingGET(id)

eventeventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventEntityApi;


EventEntityApi apiInstance = new EventEntityApi();
Long id = 789L; // Long | id
try {
    ResourceEventDef result = apiInstance.eventeventDefUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventEntityApi#eventeventDefUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

[**ResourceEventDef**](ResourceEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

<a name="eventeventDefUsingPATCH"></a>
# **eventeventDefUsingPATCH**
> ResourceEventDef eventeventDefUsingPATCH(id, body)

eventeventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventEntityApi;


EventEntityApi apiInstance = new EventEntityApi();
Long id = 789L; // Long | id
String body = "body_example"; // String | body
try {
    ResourceEventDef result = apiInstance.eventeventDefUsingPATCH(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventEntityApi#eventeventDefUsingPATCH");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | **String**| body |

### Return type

[**ResourceEventDef**](ResourceEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="eventeventDefUsingPOST"></a>
# **eventeventDefUsingPOST**
> ResourceEventDef eventeventDefUsingPOST(id, body)

eventeventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventEntityApi;


EventEntityApi apiInstance = new EventEntityApi();
Long id = 789L; // Long | id
String body = "body_example"; // String | body
try {
    ResourceEventDef result = apiInstance.eventeventDefUsingPOST(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventEntityApi#eventeventDefUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | **String**| body |

### Return type

[**ResourceEventDef**](ResourceEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="eventeventDefUsingPUT"></a>
# **eventeventDefUsingPUT**
> ResourceEventDef eventeventDefUsingPUT(id, body)

eventeventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventEntityApi;


EventEntityApi apiInstance = new EventEntityApi();
Long id = 789L; // Long | id
String body = "body_example"; // String | body
try {
    ResourceEventDef result = apiInstance.eventeventDefUsingPUT(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventEntityApi#eventeventDefUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | **String**| body |

### Return type

[**ResourceEventDef**](ResourceEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="findAllEventUsingGET"></a>
# **findAllEventUsingGET**
> ResourcesEvent findAllEventUsingGET(page, size, sort)

findAllEvent

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventEntityApi;


EventEntityApi apiInstance = new EventEntityApi();
String page = "page_example"; // String | page
String size = "size_example"; // String | size
String sort = "sort_example"; // String | sort
try {
    ResourcesEvent result = apiInstance.findAllEventUsingGET(page, size, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventEntityApi#findAllEventUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **String**| page | [optional]
 **size** | **String**| size | [optional]
 **sort** | **String**| sort | [optional]

### Return type

[**ResourcesEvent**](ResourcesEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, application/hal+json, text/uri-list, application/x-spring-data-compact+json

<a name="findOneEventUsingGET"></a>
# **findOneEventUsingGET**
> ResourceEvent findOneEventUsingGET(id)

findOneEvent

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventEntityApi;


EventEntityApi apiInstance = new EventEntityApi();
Long id = 789L; // Long | id
try {
    ResourceEvent result = apiInstance.findOneEventUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventEntityApi#findOneEventUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

[**ResourceEvent**](ResourceEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="saveEventUsingPOST"></a>
# **saveEventUsingPOST**
> ResourceEvent saveEventUsingPOST(id, body)

saveEvent

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventEntityApi;


EventEntityApi apiInstance = new EventEntityApi();
Long id = 789L; // Long | id
Event body = new Event(); // Event | body
try {
    ResourceEvent result = apiInstance.saveEventUsingPOST(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventEntityApi#saveEventUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | [**Event**](Event.md)| body |

### Return type

[**ResourceEvent**](ResourceEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="saveEventUsingPUT"></a>
# **saveEventUsingPUT**
> ResourceEvent saveEventUsingPUT(id, body)

saveEvent

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventEntityApi;


EventEntityApi apiInstance = new EventEntityApi();
Long id = 789L; // Long | id
Event body = new Event(); // Event | body
try {
    ResourceEvent result = apiInstance.saveEventUsingPUT(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventEntityApi#saveEventUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | [**Event**](Event.md)| body |

### Return type

[**ResourceEvent**](ResourceEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

