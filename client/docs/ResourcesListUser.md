
# ResourcesListUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | [**List&lt;List&lt;User&gt;&gt;**](List.md) |  |  [optional]
**links** | [**List&lt;Link&gt;**](Link.md) |  |  [optional]



