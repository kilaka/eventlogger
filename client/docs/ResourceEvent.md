
# ResourceEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eventDef** | [**EventDef**](EventDef.md) |  |  [optional]
**id** | **Long** |  |  [optional]
**links** | [**List&lt;Link&gt;**](Link.md) |  |  [optional]
**note** | **String** |  |  [optional]
**startTime** | **Long** |  |  [optional]



