# PolarEventDefEntityApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deletePolarEventDefUsingDELETE**](PolarEventDefEntityApi.md#deletePolarEventDefUsingDELETE) | **DELETE** /polarEventDefs/{id} | deletePolarEventDef
[**findAllPolarEventDefUsingGET**](PolarEventDefEntityApi.md#findAllPolarEventDefUsingGET) | **GET** /polarEventDefs | findAllPolarEventDef
[**findByUserIdPolarEventDefUsingGET**](PolarEventDefEntityApi.md#findByUserIdPolarEventDefUsingGET) | **GET** /polarEventDefs/search/findByUserId | findByUserIdPolarEventDef
[**findOnePolarEventDefUsingGET**](PolarEventDefEntityApi.md#findOnePolarEventDefUsingGET) | **GET** /polarEventDefs/{id} | findOnePolarEventDef
[**polarEventDefuserUsingDELETE**](PolarEventDefEntityApi.md#polarEventDefuserUsingDELETE) | **DELETE** /polarEventDefs/{id}/user | polarEventDefuser
[**polarEventDefuserUsingGET**](PolarEventDefEntityApi.md#polarEventDefuserUsingGET) | **GET** /polarEventDefs/{id}/user | polarEventDefuser
[**polarEventDefuserUsingPATCH**](PolarEventDefEntityApi.md#polarEventDefuserUsingPATCH) | **PATCH** /polarEventDefs/{id}/user | polarEventDefuser
[**polarEventDefuserUsingPOST**](PolarEventDefEntityApi.md#polarEventDefuserUsingPOST) | **POST** /polarEventDefs/{id}/user | polarEventDefuser
[**polarEventDefuserUsingPUT**](PolarEventDefEntityApi.md#polarEventDefuserUsingPUT) | **PUT** /polarEventDefs/{id}/user | polarEventDefuser
[**savePolarEventDefUsingPOST**](PolarEventDefEntityApi.md#savePolarEventDefUsingPOST) | **POST** /polarEventDefs | savePolarEventDef
[**savePolarEventDefUsingPUT**](PolarEventDefEntityApi.md#savePolarEventDefUsingPUT) | **PUT** /polarEventDefs/{id} | savePolarEventDef


<a name="deletePolarEventDefUsingDELETE"></a>
# **deletePolarEventDefUsingDELETE**
> deletePolarEventDefUsingDELETE(id)

deletePolarEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.PolarEventDefEntityApi;


PolarEventDefEntityApi apiInstance = new PolarEventDefEntityApi();
Long id = 789L; // Long | id
try {
    apiInstance.deletePolarEventDefUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling PolarEventDefEntityApi#deletePolarEventDefUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="findAllPolarEventDefUsingGET"></a>
# **findAllPolarEventDefUsingGET**
> ResourcesPolarEventDef findAllPolarEventDefUsingGET(page, size, sort)

findAllPolarEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.PolarEventDefEntityApi;


PolarEventDefEntityApi apiInstance = new PolarEventDefEntityApi();
String page = "page_example"; // String | page
String size = "size_example"; // String | size
String sort = "sort_example"; // String | sort
try {
    ResourcesPolarEventDef result = apiInstance.findAllPolarEventDefUsingGET(page, size, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PolarEventDefEntityApi#findAllPolarEventDefUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **String**| page | [optional]
 **size** | **String**| size | [optional]
 **sort** | **String**| sort | [optional]

### Return type

[**ResourcesPolarEventDef**](ResourcesPolarEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, application/hal+json, text/uri-list, application/x-spring-data-compact+json

<a name="findByUserIdPolarEventDefUsingGET"></a>
# **findByUserIdPolarEventDefUsingGET**
> ResourcesListEventDef findByUserIdPolarEventDefUsingGET(userId)

findByUserIdPolarEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.PolarEventDefEntityApi;


PolarEventDefEntityApi apiInstance = new PolarEventDefEntityApi();
Long userId = 789L; // Long | userId
try {
    ResourcesListEventDef result = apiInstance.findByUserIdPolarEventDefUsingGET(userId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PolarEventDefEntityApi#findByUserIdPolarEventDefUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **Long**| userId | [optional]

### Return type

[**ResourcesListEventDef**](ResourcesListEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="findOnePolarEventDefUsingGET"></a>
# **findOnePolarEventDefUsingGET**
> ResourcePolarEventDef findOnePolarEventDefUsingGET(id)

findOnePolarEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.PolarEventDefEntityApi;


PolarEventDefEntityApi apiInstance = new PolarEventDefEntityApi();
Long id = 789L; // Long | id
try {
    ResourcePolarEventDef result = apiInstance.findOnePolarEventDefUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PolarEventDefEntityApi#findOnePolarEventDefUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

[**ResourcePolarEventDef**](ResourcePolarEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="polarEventDefuserUsingDELETE"></a>
# **polarEventDefuserUsingDELETE**
> polarEventDefuserUsingDELETE(id)

polarEventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.PolarEventDefEntityApi;


PolarEventDefEntityApi apiInstance = new PolarEventDefEntityApi();
Long id = 789L; // Long | id
try {
    apiInstance.polarEventDefuserUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling PolarEventDefEntityApi#polarEventDefuserUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="polarEventDefuserUsingGET"></a>
# **polarEventDefuserUsingGET**
> ResourceUser polarEventDefuserUsingGET(id)

polarEventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.PolarEventDefEntityApi;


PolarEventDefEntityApi apiInstance = new PolarEventDefEntityApi();
Long id = 789L; // Long | id
try {
    ResourceUser result = apiInstance.polarEventDefuserUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PolarEventDefEntityApi#polarEventDefuserUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

<a name="polarEventDefuserUsingPATCH"></a>
# **polarEventDefuserUsingPATCH**
> ResourceUser polarEventDefuserUsingPATCH(id, body)

polarEventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.PolarEventDefEntityApi;


PolarEventDefEntityApi apiInstance = new PolarEventDefEntityApi();
Long id = 789L; // Long | id
String body = "body_example"; // String | body
try {
    ResourceUser result = apiInstance.polarEventDefuserUsingPATCH(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PolarEventDefEntityApi#polarEventDefuserUsingPATCH");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | **String**| body |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="polarEventDefuserUsingPOST"></a>
# **polarEventDefuserUsingPOST**
> ResourceUser polarEventDefuserUsingPOST(id, body)

polarEventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.PolarEventDefEntityApi;


PolarEventDefEntityApi apiInstance = new PolarEventDefEntityApi();
Long id = 789L; // Long | id
String body = "body_example"; // String | body
try {
    ResourceUser result = apiInstance.polarEventDefuserUsingPOST(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PolarEventDefEntityApi#polarEventDefuserUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | **String**| body |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="polarEventDefuserUsingPUT"></a>
# **polarEventDefuserUsingPUT**
> ResourceUser polarEventDefuserUsingPUT(id, body)

polarEventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.PolarEventDefEntityApi;


PolarEventDefEntityApi apiInstance = new PolarEventDefEntityApi();
Long id = 789L; // Long | id
String body = "body_example"; // String | body
try {
    ResourceUser result = apiInstance.polarEventDefuserUsingPUT(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PolarEventDefEntityApi#polarEventDefuserUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | **String**| body |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="savePolarEventDefUsingPOST"></a>
# **savePolarEventDefUsingPOST**
> ResourcePolarEventDef savePolarEventDefUsingPOST(id, body)

savePolarEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.PolarEventDefEntityApi;


PolarEventDefEntityApi apiInstance = new PolarEventDefEntityApi();
Long id = 789L; // Long | id
PolarEventDef body = new PolarEventDef(); // PolarEventDef | body
try {
    ResourcePolarEventDef result = apiInstance.savePolarEventDefUsingPOST(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PolarEventDefEntityApi#savePolarEventDefUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | [**PolarEventDef**](PolarEventDef.md)| body |

### Return type

[**ResourcePolarEventDef**](ResourcePolarEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="savePolarEventDefUsingPUT"></a>
# **savePolarEventDefUsingPUT**
> ResourcePolarEventDef savePolarEventDefUsingPUT(id, body)

savePolarEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.PolarEventDefEntityApi;


PolarEventDefEntityApi apiInstance = new PolarEventDefEntityApi();
Long id = 789L; // Long | id
PolarEventDef body = new PolarEventDef(); // PolarEventDef | body
try {
    ResourcePolarEventDef result = apiInstance.savePolarEventDefUsingPUT(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PolarEventDefEntityApi#savePolarEventDefUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | [**PolarEventDef**](PolarEventDef.md)| body |

### Return type

[**ResourcePolarEventDef**](ResourcePolarEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

