
# ResourcesListEventDef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | [**List&lt;List&lt;EventDef&gt;&gt;**](List.md) |  |  [optional]
**links** | [**List&lt;Link&gt;**](Link.md) |  |  [optional]



