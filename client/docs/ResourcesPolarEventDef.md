
# ResourcesPolarEventDef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | [**List&lt;PolarEventDef&gt;**](PolarEventDef.md) |  |  [optional]
**links** | [**List&lt;Link&gt;**](Link.md) |  |  [optional]



