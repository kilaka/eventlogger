
# ResourcePolarEventDef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**links** | [**List&lt;Link&gt;**](Link.md) |  |  [optional]
**name** | **String** |  |  [optional]
**northName** | **String** |  |  [optional]
**southName** | **String** |  |  [optional]
**updateTime** | **Long** |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]



