# InstantEventDefEntityApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteInstantEventDefUsingDELETE**](InstantEventDefEntityApi.md#deleteInstantEventDefUsingDELETE) | **DELETE** /instantEventDefs/{id} | deleteInstantEventDef
[**findAllInstantEventDefUsingGET**](InstantEventDefEntityApi.md#findAllInstantEventDefUsingGET) | **GET** /instantEventDefs | findAllInstantEventDef
[**findByUserIdInstantEventDefUsingGET**](InstantEventDefEntityApi.md#findByUserIdInstantEventDefUsingGET) | **GET** /instantEventDefs/search/findByUserId | findByUserIdInstantEventDef
[**findOneInstantEventDefUsingGET**](InstantEventDefEntityApi.md#findOneInstantEventDefUsingGET) | **GET** /instantEventDefs/{id} | findOneInstantEventDef
[**instantEventDefuserUsingDELETE**](InstantEventDefEntityApi.md#instantEventDefuserUsingDELETE) | **DELETE** /instantEventDefs/{id}/user | instantEventDefuser
[**instantEventDefuserUsingGET**](InstantEventDefEntityApi.md#instantEventDefuserUsingGET) | **GET** /instantEventDefs/{id}/user | instantEventDefuser
[**instantEventDefuserUsingPATCH**](InstantEventDefEntityApi.md#instantEventDefuserUsingPATCH) | **PATCH** /instantEventDefs/{id}/user | instantEventDefuser
[**instantEventDefuserUsingPOST**](InstantEventDefEntityApi.md#instantEventDefuserUsingPOST) | **POST** /instantEventDefs/{id}/user | instantEventDefuser
[**instantEventDefuserUsingPUT**](InstantEventDefEntityApi.md#instantEventDefuserUsingPUT) | **PUT** /instantEventDefs/{id}/user | instantEventDefuser
[**saveInstantEventDefUsingPOST**](InstantEventDefEntityApi.md#saveInstantEventDefUsingPOST) | **POST** /instantEventDefs | saveInstantEventDef
[**saveInstantEventDefUsingPUT**](InstantEventDefEntityApi.md#saveInstantEventDefUsingPUT) | **PUT** /instantEventDefs/{id} | saveInstantEventDef


<a name="deleteInstantEventDefUsingDELETE"></a>
# **deleteInstantEventDefUsingDELETE**
> deleteInstantEventDefUsingDELETE(id)

deleteInstantEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InstantEventDefEntityApi;


InstantEventDefEntityApi apiInstance = new InstantEventDefEntityApi();
Long id = 789L; // Long | id
try {
    apiInstance.deleteInstantEventDefUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling InstantEventDefEntityApi#deleteInstantEventDefUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="findAllInstantEventDefUsingGET"></a>
# **findAllInstantEventDefUsingGET**
> ResourcesInstantEventDef findAllInstantEventDefUsingGET(page, size, sort)

findAllInstantEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InstantEventDefEntityApi;


InstantEventDefEntityApi apiInstance = new InstantEventDefEntityApi();
String page = "page_example"; // String | page
String size = "size_example"; // String | size
String sort = "sort_example"; // String | sort
try {
    ResourcesInstantEventDef result = apiInstance.findAllInstantEventDefUsingGET(page, size, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InstantEventDefEntityApi#findAllInstantEventDefUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **String**| page | [optional]
 **size** | **String**| size | [optional]
 **sort** | **String**| sort | [optional]

### Return type

[**ResourcesInstantEventDef**](ResourcesInstantEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, application/hal+json, text/uri-list, application/x-spring-data-compact+json

<a name="findByUserIdInstantEventDefUsingGET"></a>
# **findByUserIdInstantEventDefUsingGET**
> ResourcesListEventDef findByUserIdInstantEventDefUsingGET(userId)

findByUserIdInstantEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InstantEventDefEntityApi;


InstantEventDefEntityApi apiInstance = new InstantEventDefEntityApi();
Long userId = 789L; // Long | userId
try {
    ResourcesListEventDef result = apiInstance.findByUserIdInstantEventDefUsingGET(userId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InstantEventDefEntityApi#findByUserIdInstantEventDefUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **Long**| userId | [optional]

### Return type

[**ResourcesListEventDef**](ResourcesListEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="findOneInstantEventDefUsingGET"></a>
# **findOneInstantEventDefUsingGET**
> ResourceInstantEventDef findOneInstantEventDefUsingGET(id)

findOneInstantEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InstantEventDefEntityApi;


InstantEventDefEntityApi apiInstance = new InstantEventDefEntityApi();
Long id = 789L; // Long | id
try {
    ResourceInstantEventDef result = apiInstance.findOneInstantEventDefUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InstantEventDefEntityApi#findOneInstantEventDefUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

[**ResourceInstantEventDef**](ResourceInstantEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="instantEventDefuserUsingDELETE"></a>
# **instantEventDefuserUsingDELETE**
> instantEventDefuserUsingDELETE(id)

instantEventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InstantEventDefEntityApi;


InstantEventDefEntityApi apiInstance = new InstantEventDefEntityApi();
Long id = 789L; // Long | id
try {
    apiInstance.instantEventDefuserUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling InstantEventDefEntityApi#instantEventDefuserUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="instantEventDefuserUsingGET"></a>
# **instantEventDefuserUsingGET**
> ResourceUser instantEventDefuserUsingGET(id)

instantEventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InstantEventDefEntityApi;


InstantEventDefEntityApi apiInstance = new InstantEventDefEntityApi();
Long id = 789L; // Long | id
try {
    ResourceUser result = apiInstance.instantEventDefuserUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InstantEventDefEntityApi#instantEventDefuserUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

<a name="instantEventDefuserUsingPATCH"></a>
# **instantEventDefuserUsingPATCH**
> ResourceUser instantEventDefuserUsingPATCH(id, body)

instantEventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InstantEventDefEntityApi;


InstantEventDefEntityApi apiInstance = new InstantEventDefEntityApi();
Long id = 789L; // Long | id
String body = "body_example"; // String | body
try {
    ResourceUser result = apiInstance.instantEventDefuserUsingPATCH(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InstantEventDefEntityApi#instantEventDefuserUsingPATCH");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | **String**| body |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="instantEventDefuserUsingPOST"></a>
# **instantEventDefuserUsingPOST**
> ResourceUser instantEventDefuserUsingPOST(id, body)

instantEventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InstantEventDefEntityApi;


InstantEventDefEntityApi apiInstance = new InstantEventDefEntityApi();
Long id = 789L; // Long | id
String body = "body_example"; // String | body
try {
    ResourceUser result = apiInstance.instantEventDefuserUsingPOST(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InstantEventDefEntityApi#instantEventDefuserUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | **String**| body |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="instantEventDefuserUsingPUT"></a>
# **instantEventDefuserUsingPUT**
> ResourceUser instantEventDefuserUsingPUT(id, body)

instantEventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InstantEventDefEntityApi;


InstantEventDefEntityApi apiInstance = new InstantEventDefEntityApi();
Long id = 789L; // Long | id
String body = "body_example"; // String | body
try {
    ResourceUser result = apiInstance.instantEventDefuserUsingPUT(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InstantEventDefEntityApi#instantEventDefuserUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | **String**| body |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="saveInstantEventDefUsingPOST"></a>
# **saveInstantEventDefUsingPOST**
> ResourceInstantEventDef saveInstantEventDefUsingPOST(id, body)

saveInstantEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InstantEventDefEntityApi;


InstantEventDefEntityApi apiInstance = new InstantEventDefEntityApi();
Long id = 789L; // Long | id
InstantEventDef body = new InstantEventDef(); // InstantEventDef | body
try {
    ResourceInstantEventDef result = apiInstance.saveInstantEventDefUsingPOST(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InstantEventDefEntityApi#saveInstantEventDefUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | [**InstantEventDef**](InstantEventDef.md)| body |

### Return type

[**ResourceInstantEventDef**](ResourceInstantEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="saveInstantEventDefUsingPUT"></a>
# **saveInstantEventDefUsingPUT**
> ResourceInstantEventDef saveInstantEventDefUsingPUT(id, body)

saveInstantEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InstantEventDefEntityApi;


InstantEventDefEntityApi apiInstance = new InstantEventDefEntityApi();
Long id = 789L; // Long | id
InstantEventDef body = new InstantEventDef(); // InstantEventDef | body
try {
    ResourceInstantEventDef result = apiInstance.saveInstantEventDefUsingPUT(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InstantEventDefEntityApi#saveInstantEventDefUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | [**InstantEventDef**](InstantEventDef.md)| body |

### Return type

[**ResourceInstantEventDef**](ResourceInstantEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

