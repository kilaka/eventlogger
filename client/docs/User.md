
# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | [**Company**](Company.md) |  |  [optional]
**firstName** | **String** |  |  [optional]
**id** | **Long** |  |  [optional]
**lastName** | **String** |  |  [optional]
**username** | **String** |  |  [optional]



