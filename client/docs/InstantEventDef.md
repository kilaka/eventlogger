
# InstantEventDef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**updateTime** | **Long** |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]



