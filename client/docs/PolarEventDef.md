
# PolarEventDef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**northName** | **String** |  |  [optional]
**southName** | **String** |  |  [optional]
**updateTime** | **Long** |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]



