# EventDefEntityApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteEventDefUsingDELETE**](EventDefEntityApi.md#deleteEventDefUsingDELETE) | **DELETE** /eventDefs/{id} | deleteEventDef
[**eventDefuserUsingDELETE**](EventDefEntityApi.md#eventDefuserUsingDELETE) | **DELETE** /eventDefs/{id}/user | eventDefuser
[**eventDefuserUsingGET**](EventDefEntityApi.md#eventDefuserUsingGET) | **GET** /eventDefs/{id}/user | eventDefuser
[**eventDefuserUsingPATCH**](EventDefEntityApi.md#eventDefuserUsingPATCH) | **PATCH** /eventDefs/{id}/user | eventDefuser
[**eventDefuserUsingPOST**](EventDefEntityApi.md#eventDefuserUsingPOST) | **POST** /eventDefs/{id}/user | eventDefuser
[**eventDefuserUsingPUT**](EventDefEntityApi.md#eventDefuserUsingPUT) | **PUT** /eventDefs/{id}/user | eventDefuser
[**findAllEventDefUsingGET**](EventDefEntityApi.md#findAllEventDefUsingGET) | **GET** /eventDefs | findAllEventDef
[**findByUserIdEventDefUsingGET**](EventDefEntityApi.md#findByUserIdEventDefUsingGET) | **GET** /eventDefs/search/findByUserId | findByUserIdEventDef
[**findOneEventDefUsingGET**](EventDefEntityApi.md#findOneEventDefUsingGET) | **GET** /eventDefs/{id} | findOneEventDef
[**saveEventDefUsingPOST**](EventDefEntityApi.md#saveEventDefUsingPOST) | **POST** /eventDefs | saveEventDef
[**saveEventDefUsingPUT**](EventDefEntityApi.md#saveEventDefUsingPUT) | **PUT** /eventDefs/{id} | saveEventDef


<a name="deleteEventDefUsingDELETE"></a>
# **deleteEventDefUsingDELETE**
> deleteEventDefUsingDELETE(id)

deleteEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventDefEntityApi;


EventDefEntityApi apiInstance = new EventDefEntityApi();
Long id = 789L; // Long | id
try {
    apiInstance.deleteEventDefUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling EventDefEntityApi#deleteEventDefUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="eventDefuserUsingDELETE"></a>
# **eventDefuserUsingDELETE**
> eventDefuserUsingDELETE(id)

eventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventDefEntityApi;


EventDefEntityApi apiInstance = new EventDefEntityApi();
Long id = 789L; // Long | id
try {
    apiInstance.eventDefuserUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling EventDefEntityApi#eventDefuserUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="eventDefuserUsingGET"></a>
# **eventDefuserUsingGET**
> ResourceUser eventDefuserUsingGET(id)

eventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventDefEntityApi;


EventDefEntityApi apiInstance = new EventDefEntityApi();
Long id = 789L; // Long | id
try {
    ResourceUser result = apiInstance.eventDefuserUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventDefEntityApi#eventDefuserUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

<a name="eventDefuserUsingPATCH"></a>
# **eventDefuserUsingPATCH**
> ResourceUser eventDefuserUsingPATCH(id, body)

eventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventDefEntityApi;


EventDefEntityApi apiInstance = new EventDefEntityApi();
Long id = 789L; // Long | id
String body = "body_example"; // String | body
try {
    ResourceUser result = apiInstance.eventDefuserUsingPATCH(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventDefEntityApi#eventDefuserUsingPATCH");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | **String**| body |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="eventDefuserUsingPOST"></a>
# **eventDefuserUsingPOST**
> ResourceUser eventDefuserUsingPOST(id, body)

eventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventDefEntityApi;


EventDefEntityApi apiInstance = new EventDefEntityApi();
Long id = 789L; // Long | id
String body = "body_example"; // String | body
try {
    ResourceUser result = apiInstance.eventDefuserUsingPOST(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventDefEntityApi#eventDefuserUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | **String**| body |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="eventDefuserUsingPUT"></a>
# **eventDefuserUsingPUT**
> ResourceUser eventDefuserUsingPUT(id, body)

eventDefuser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventDefEntityApi;


EventDefEntityApi apiInstance = new EventDefEntityApi();
Long id = 789L; // Long | id
String body = "body_example"; // String | body
try {
    ResourceUser result = apiInstance.eventDefuserUsingPUT(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventDefEntityApi#eventDefuserUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | **String**| body |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/uri-list, application/x-spring-data-compact+json
 - **Accept**: */*

<a name="findAllEventDefUsingGET"></a>
# **findAllEventDefUsingGET**
> ResourcesEventDef findAllEventDefUsingGET(page, size, sort)

findAllEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventDefEntityApi;


EventDefEntityApi apiInstance = new EventDefEntityApi();
String page = "page_example"; // String | page
String size = "size_example"; // String | size
String sort = "sort_example"; // String | sort
try {
    ResourcesEventDef result = apiInstance.findAllEventDefUsingGET(page, size, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventDefEntityApi#findAllEventDefUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **String**| page | [optional]
 **size** | **String**| size | [optional]
 **sort** | **String**| sort | [optional]

### Return type

[**ResourcesEventDef**](ResourcesEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, application/hal+json, text/uri-list, application/x-spring-data-compact+json

<a name="findByUserIdEventDefUsingGET"></a>
# **findByUserIdEventDefUsingGET**
> ResourcesListEventDef findByUserIdEventDefUsingGET(userId)

findByUserIdEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventDefEntityApi;


EventDefEntityApi apiInstance = new EventDefEntityApi();
Long userId = 789L; // Long | userId
try {
    ResourcesListEventDef result = apiInstance.findByUserIdEventDefUsingGET(userId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventDefEntityApi#findByUserIdEventDefUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **Long**| userId | [optional]

### Return type

[**ResourcesListEventDef**](ResourcesListEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="findOneEventDefUsingGET"></a>
# **findOneEventDefUsingGET**
> ResourceEventDef findOneEventDefUsingGET(id)

findOneEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventDefEntityApi;


EventDefEntityApi apiInstance = new EventDefEntityApi();
Long id = 789L; // Long | id
try {
    ResourceEventDef result = apiInstance.findOneEventDefUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventDefEntityApi#findOneEventDefUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

[**ResourceEventDef**](ResourceEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="saveEventDefUsingPOST"></a>
# **saveEventDefUsingPOST**
> ResourceEventDef saveEventDefUsingPOST(id, body)

saveEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventDefEntityApi;


EventDefEntityApi apiInstance = new EventDefEntityApi();
Long id = 789L; // Long | id
EventDef body = new EventDef(); // EventDef | body
try {
    ResourceEventDef result = apiInstance.saveEventDefUsingPOST(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventDefEntityApi#saveEventDefUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | [**EventDef**](EventDef.md)| body |

### Return type

[**ResourceEventDef**](ResourceEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="saveEventDefUsingPUT"></a>
# **saveEventDefUsingPUT**
> ResourceEventDef saveEventDefUsingPUT(id, body)

saveEventDef

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EventDefEntityApi;


EventDefEntityApi apiInstance = new EventDefEntityApi();
Long id = 789L; // Long | id
EventDef body = new EventDef(); // EventDef | body
try {
    ResourceEventDef result = apiInstance.saveEventDefUsingPUT(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventDefEntityApi#saveEventDefUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | [**EventDef**](EventDef.md)| body |

### Return type

[**ResourceEventDef**](ResourceEventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

