# RestServiceApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**findAllUsingGET**](RestServiceApi.md#findAllUsingGET) | **GET** /api | findAll
[**patchAllUsingPATCH**](RestServiceApi.md#patchAllUsingPATCH) | **PATCH** /api | patchAll


<a name="findAllUsingGET"></a>
# **findAllUsingGET**
> List&lt;EventDef&gt; findAllUsingGET()

findAll

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.RestServiceApi;


RestServiceApi apiInstance = new RestServiceApi();
try {
    List<EventDef> result = apiInstance.findAllUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RestServiceApi#findAllUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;EventDef&gt;**](EventDef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="patchAllUsingPATCH"></a>
# **patchAllUsingPATCH**
> patchAllUsingPATCH()

patchAll

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.RestServiceApi;


RestServiceApi apiInstance = new RestServiceApi();
try {
    apiInstance.patchAllUsingPATCH();
} catch (ApiException e) {
    System.err.println("Exception when calling RestServiceApi#patchAllUsingPATCH");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

