
# Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eventDef** | [**EventDef**](EventDef.md) |  |  [optional]
**id** | **Long** |  |  [optional]
**note** | **String** |  |  [optional]
**startTime** | **Long** |  |  [optional]



