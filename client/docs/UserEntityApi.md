# UserEntityApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteUserUsingDELETE**](UserEntityApi.md#deleteUserUsingDELETE) | **DELETE** /users/{id} | deleteUser
[**findAllUserUsingGET**](UserEntityApi.md#findAllUserUsingGET) | **GET** /users | findAllUser
[**findByFirstNameLikeUserUsingGET**](UserEntityApi.md#findByFirstNameLikeUserUsingGET) | **GET** /users/search/findByFirstNameLike | findByFirstNameLikeUser
[**findByFirstNameUserUsingGET**](UserEntityApi.md#findByFirstNameUserUsingGET) | **GET** /users/search/findByFirstName | findByFirstNameUser
[**findByUsernameUserUsingGET**](UserEntityApi.md#findByUsernameUserUsingGET) | **GET** /users/search/findByUsername | findByUsernameUser
[**findOneUserUsingGET**](UserEntityApi.md#findOneUserUsingGET) | **GET** /users/{id} | findOneUser
[**saveUserUsingPOST**](UserEntityApi.md#saveUserUsingPOST) | **POST** /users | saveUser
[**saveUserUsingPUT**](UserEntityApi.md#saveUserUsingPUT) | **PUT** /users/{id} | saveUser


<a name="deleteUserUsingDELETE"></a>
# **deleteUserUsingDELETE**
> deleteUserUsingDELETE(id)

deleteUser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UserEntityApi;


UserEntityApi apiInstance = new UserEntityApi();
Long id = 789L; // Long | id
try {
    apiInstance.deleteUserUsingDELETE(id);
} catch (ApiException e) {
    System.err.println("Exception when calling UserEntityApi#deleteUserUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="findAllUserUsingGET"></a>
# **findAllUserUsingGET**
> ResourcesUser findAllUserUsingGET(page, size, sort)

findAllUser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UserEntityApi;


UserEntityApi apiInstance = new UserEntityApi();
String page = "page_example"; // String | page
String size = "size_example"; // String | size
String sort = "sort_example"; // String | sort
try {
    ResourcesUser result = apiInstance.findAllUserUsingGET(page, size, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserEntityApi#findAllUserUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **String**| page | [optional]
 **size** | **String**| size | [optional]
 **sort** | **String**| sort | [optional]

### Return type

[**ResourcesUser**](ResourcesUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, application/hal+json, text/uri-list, application/x-spring-data-compact+json

<a name="findByFirstNameLikeUserUsingGET"></a>
# **findByFirstNameLikeUserUsingGET**
> ResourcesListUser findByFirstNameLikeUserUsingGET(firstName)

findByFirstNameLikeUser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UserEntityApi;


UserEntityApi apiInstance = new UserEntityApi();
String firstName = "firstName_example"; // String | firstName
try {
    ResourcesListUser result = apiInstance.findByFirstNameLikeUserUsingGET(firstName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserEntityApi#findByFirstNameLikeUserUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **firstName** | **String**| firstName | [optional]

### Return type

[**ResourcesListUser**](ResourcesListUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="findByFirstNameUserUsingGET"></a>
# **findByFirstNameUserUsingGET**
> ResourcesListUser findByFirstNameUserUsingGET(firstName)

findByFirstNameUser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UserEntityApi;


UserEntityApi apiInstance = new UserEntityApi();
String firstName = "firstName_example"; // String | firstName
try {
    ResourcesListUser result = apiInstance.findByFirstNameUserUsingGET(firstName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserEntityApi#findByFirstNameUserUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **firstName** | **String**| firstName | [optional]

### Return type

[**ResourcesListUser**](ResourcesListUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="findByUsernameUserUsingGET"></a>
# **findByUsernameUserUsingGET**
> ResourceUser findByUsernameUserUsingGET(username)

findByUsernameUser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UserEntityApi;


UserEntityApi apiInstance = new UserEntityApi();
String username = "username_example"; // String | username
try {
    ResourceUser result = apiInstance.findByUsernameUserUsingGET(username);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserEntityApi#findByUsernameUserUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| username | [optional]

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="findOneUserUsingGET"></a>
# **findOneUserUsingGET**
> ResourceUser findOneUserUsingGET(id)

findOneUser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UserEntityApi;


UserEntityApi apiInstance = new UserEntityApi();
Long id = 789L; // Long | id
try {
    ResourceUser result = apiInstance.findOneUserUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserEntityApi#findOneUserUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="saveUserUsingPOST"></a>
# **saveUserUsingPOST**
> ResourceUser saveUserUsingPOST(id, body)

saveUser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UserEntityApi;


UserEntityApi apiInstance = new UserEntityApi();
Long id = 789L; // Long | id
User body = new User(); // User | body
try {
    ResourceUser result = apiInstance.saveUserUsingPOST(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserEntityApi#saveUserUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | [**User**](User.md)| body |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="saveUserUsingPUT"></a>
# **saveUserUsingPUT**
> ResourceUser saveUserUsingPUT(id, body)

saveUser

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UserEntityApi;


UserEntityApi apiInstance = new UserEntityApi();
Long id = 789L; // Long | id
User body = new User(); // User | body
try {
    ResourceUser result = apiInstance.saveUserUsingPUT(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserEntityApi#saveUserUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **body** | [**User**](User.md)| body |

### Return type

[**ResourceUser**](ResourceUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

