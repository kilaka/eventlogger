
# ResourcesEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | [**List&lt;Event&gt;**](Event.md) |  |  [optional]
**links** | [**List&lt;Link&gt;**](Link.md) |  |  [optional]



