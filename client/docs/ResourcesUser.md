
# ResourcesUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | [**List&lt;User&gt;**](User.md) |  |  [optional]
**links** | [**List&lt;Link&gt;**](Link.md) |  |  [optional]



