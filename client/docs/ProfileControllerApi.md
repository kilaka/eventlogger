# ProfileControllerApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listAllFormsOfMetadataUsingGET**](ProfileControllerApi.md#listAllFormsOfMetadataUsingGET) | **GET** /profile | listAllFormsOfMetadata
[**profileOptionsUsingOPTIONS**](ProfileControllerApi.md#profileOptionsUsingOPTIONS) | **OPTIONS** /profile | profileOptions


<a name="listAllFormsOfMetadataUsingGET"></a>
# **listAllFormsOfMetadataUsingGET**
> ResourceSupport listAllFormsOfMetadataUsingGET()

listAllFormsOfMetadata

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ProfileControllerApi;


ProfileControllerApi apiInstance = new ProfileControllerApi();
try {
    ResourceSupport result = apiInstance.listAllFormsOfMetadataUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileControllerApi#listAllFormsOfMetadataUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResourceSupport**](ResourceSupport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="profileOptionsUsingOPTIONS"></a>
# **profileOptionsUsingOPTIONS**
> Object profileOptionsUsingOPTIONS()

profileOptions

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ProfileControllerApi;


ProfileControllerApi apiInstance = new ProfileControllerApi();
try {
    Object result = apiInstance.profileOptionsUsingOPTIONS();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileControllerApi#profileOptionsUsingOPTIONS");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

