package alik.eventlogger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.validation.constraints.NotNull;
import java.util.List;

//@NoRepositoryBean
@RepositoryRestResource
public interface EventDefRepository<T extends EventDef> extends JpaRepository<T, Long> {

	@Override
	@PreAuthorize("hasRole('ADMIN')")
////	@PostAuthorize("")
	// TODO: Check why not working from rest.
	List<T> findAll();

	//	@PostAuthorize("returnObject.forEach()")
	//	@PostAuthorize("principal.username.equals(#userId)")
//	@PreAuthorize("@securityService.isAdmin()")
	@PreAuthorize("@securityConfiguration.isAdmin() or @securityConfiguration.isAuth(#userId)")
	List<T> findByUserId(Long userId);

	// Not working:
	// Request: curl -i http://localhost:8080/eventDefs/search/findByUserFirstName?firstName=Alik
	// Response: {"timestamp":1520213143497,"status":500,"error":"Internal Server Error","exception":"org.springframework.data.mapping.model.MappingException","message":"Invalid path reference user.firstName! Associations can only be pointed to directly or via their id property!","path":"/eventDefs/search/findByUserFirstName"}
	// List<T> findByUserFirstName(String firstName);
}
