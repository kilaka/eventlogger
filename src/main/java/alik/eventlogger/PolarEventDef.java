package alik.eventlogger;

import javax.persistence.Entity;

@Entity
public class PolarEventDef extends EventDef {

	public String northName;
	public String southName;
	enum Which {first, second}

}
