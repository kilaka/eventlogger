package alik.eventlogger;

import javax.persistence.*;

@Entity
public class User {

	@Id
	@GeneratedValue
	private Long id;

	private  String username;

	private String firstName;
	private String lastName;

//	public Address address;


//	@OneToMany(mappedBy = "user", targetEntity = EventDef.class)
//	public List<EventDef> eventDefs;

	@ManyToOne
	private Company company;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public User setUsername(String username) {
		this.username = username;
		return this;
	}

	public String getFirstName() {
		return firstName;
	}

	public User setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public User setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public Company getCompany() {
		return company;
	}

	public User setCompany(Company company) {
		this.company = company;
		return this;
	}
}
