package alik.eventlogger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
				.withUser("kilaka").password("password").roles("USER").and()
				.withUser("orlyof").password("password").roles("USER").and()
				.withUser("admin").password("password").roles("USER", "ADMIN");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().anyRequest().fullyAuthenticated();
		http.httpBasic();
		http.csrf().disable();
		// Needed for the h2-console to show content.
		http.headers().frameOptions().sameOrigin();
	}


	@Autowired
	private UserRepository userRepository;

	public boolean isAdmin() {
		UserDetails user = getPrincipal();
		return user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(s -> s.equals("ROLE_ADMIN"));
	}

	private UserDetails getPrincipal() {
		return (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	/**
	 * TODO replace in the future with scopes and ownership checks.
	 */
	public boolean isAuth(Long userId) {
		UserDetails userDetails = getPrincipal();
		User user = userRepository.findByUsername(userDetails.getUsername());
		return userId.equals(user.getId());
	}
}