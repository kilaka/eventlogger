package alik.eventlogger;

import java.util.List;

//@NoRepositoryBean
public interface PolarEventDefRepository<T extends PolarEventDef> extends EventDefRepository<T> {

	@Override
//	@Query(value= "{\"_class\": \"alik.eventlogger.PolarEventDef\"}")
	List<T> findAll();
}
