package alik.eventlogger;


import javax.persistence.*;

@Entity
public abstract class EventDef {

	@Id
	@GeneratedValue
	public Long id;

	public String name;

	@ManyToOne
	public User user;

	public Long updateTime;


}
