package alik.eventlogger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/api"})
// Transactional doesn't work with mongo.
// @Transactional
public class RestService {

	@Autowired
	private EventDefRepository<EventDef> eventDefEventDefRepository;

//	@GetMapping
//	public List<Order> findAll() {
//		List<Order> orders = orderRepository.findAll();
//		return orders;
//	}
	@GetMapping
	public List<EventDef> findAll() {
		List<EventDef> all = eventDefEventDefRepository.findAll();
		// Returns the exact same instance of user for eventDefs with same userIds.
		return all;
	}

	@PatchMapping
	public void patchAll() {
		List<EventDef> all = eventDefEventDefRepository.findAll();
		all.forEach(e -> {
			e.updateTime = System.currentTimeMillis();
			eventDefEventDefRepository.save(e);
		});
	}

}
