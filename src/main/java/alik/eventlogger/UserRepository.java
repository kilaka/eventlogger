package alik.eventlogger;

import io.swagger.annotations.Api;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

//@Api
//@RepositoryRestResource
@PreAuthorize("@securityConfiguration.isAdmin()")
public interface UserRepository extends JpaRepository<User, Long> {

	@Override
	@PreAuthorize("hasRole('ADMIN')")
	Page<User> findAll(Pageable pageable);

//    @Override
//    @PostAuthorize("returnObject.firstName == principal.username or hasRole('ROLE_ADMIN')")
//    Person findOne(Long aLong);

	@PreAuthorize("hasRole('ADMIN')")
	List<User> findByFirstNameLike(String firstName);


	@PreAuthorize("hasRole('ADMIN')")
	List<User> findByFirstName(String firstName);

//	List<User> findByAddressCity(String city);

//    @Query("select u from User u where u.eventDefs.name like ?1")
//    List<User> findByEventDefsName(String name);

	@PreAuthorize("@securityConfiguration.isAdmin()")
//	@PreAuthorize("hasRole('ADMIN')")
	User findByUsername(String username);

//    List<Customer> findByLastName(String lastName);


}
