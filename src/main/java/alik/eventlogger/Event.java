package alik.eventlogger;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public abstract class Event<T extends EventDef> {

	@Id
	@GeneratedValue
	public Long id;

	public long startTime;
	public String note;

	@ManyToOne(targetEntity = EventDef.class)
	public T eventDef;

}
