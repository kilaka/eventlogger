package alik.eventlogger;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

@RunWith(SpringRunner.class)
//@DataJpaTest
@SpringBootTest
public class UserRepoTest {

//	@Autowired
//	private TestEntityManager entityManager;

	@Autowired
	private UserRepository userRepository;

	@Test
	public void testFindByUsername() {
		// given
		String username = "kilaka";
		User user = new User()
				.setFirstName("Alik")
				.setLastName("Elzin")
				.setUsername(username);

//		entityManager.persist(user);
//		entityManager.flush();

		try {
			SecurityContext ctx = SecurityContextHolder.createEmptyContext();
			SecurityContextHolder.setContext(ctx);
			org.springframework.security.core.userdetails.User principal = new org.springframework.security.core.userdetails.User("admin", "password", Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN")));
			UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(principal, null, principal.getAuthorities());
//			authenticationToken.setAuthenticated(true);
			ctx.setAuthentication(authenticationToken);

			userRepository.save(user);
			userRepository.flush();

			// when
			User found = userRepository.findByUsername(username);

			// then
//		Assert.assertEquals(user.getFirstName(), found.getFirstName());
			Assertions.assertThat(user.getFirstName()).isEqualTo(found.getFirstName());

		} finally {
			SecurityContextHolder.clearContext();
		}


	}
}
